package com.example.mvpregistration.ui.homePage

import com.example.mvpregistration.Models.Error
import com.example.mvpregistration.Models.homePage.productResponse

interface IProductView {

    fun onProductSuccess(loginBase: productResponse)
    fun onProductError(error: Error)

}