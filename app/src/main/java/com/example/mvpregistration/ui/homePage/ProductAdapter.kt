package com.example.mvpregistration.ui.homePage

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.mvpregistration.Models.homePage.productData
import com.example.mvpregistration.R
import com.google.gson.JsonObject
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.items.view.*

class ProductAdapter(private val dataList: List<productData>) : RecyclerView.Adapter<MyViewHolder>(){

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        context = parent.context
        return MyViewHolder(
            LayoutInflater.from(
                context
            ).inflate(
                R.layout.items,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = dataList[position]

        val productName = holder.itemView.titleHomeTv
        val productImage = holder.itemView.imageHomeimgID
        val productPrice = holder.itemView.priceHometextView

        val ItemName= data.product_name
        productName.text = ItemName
        val ItemPrice = data.price
        productPrice.text = "₹"+ItemPrice

        Picasso.get()
            .load(data.image_url)
            .into(productImage)

        holder.itemView.setOnClickListener {
            Toast.makeText(context,ItemName, Toast.LENGTH_SHORT).show()
        }
    }
}