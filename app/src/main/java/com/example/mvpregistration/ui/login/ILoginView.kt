package com.example.mvpregistration.ui.login

import com.example.mvpregistration.ui.splashScreen.MvpView
import com.example.mvpregistration.Models.UsersList

interface ILoginView: MvpView {
    fun onSuccess(loginBase: UsersList)
    fun onError(error: Error)
}