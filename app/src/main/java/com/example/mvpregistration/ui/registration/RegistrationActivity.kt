package com.example.mvpregistration.ui.registration

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.example.mvpregistration.Models.*
import com.example.mvpregistration.Models.registration.*
import com.example.mvpregistration.R
import com.example.mvpregistration.ui.login.MainActivity
import com.example.mvpregistration.ui.splashScreen.BaseActivity
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_registration.*
import retrofit2.Response

class RegistrationActivity : BaseActivity(), IRegisterView,AdapterView.OnItemSelectedListener {

    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()
    private lateinit var countryList : Array<CountryDetails>
    private lateinit var stateList : Array<StateDetails>
    private lateinit var cityList : Array<CityDetails>
    private  var cntryId :Int = 0
    private  var stateId:Int = 0
    private  var cityId:Int  = 0

    private val RegistrationPresenter= RegistrationPresenter(this,this)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        progressBarbaseactivity2.visibility = View.GONE

        spinnerCountry.onItemSelectedListener = this
        spinnerState.onItemSelectedListener = this
        spinnerCity.onItemSelectedListener=this

        registerRegButton.setOnClickListener {
            var firstName = editTextTextFirstName.text.toString()
            var lastName = editTextTextLastName.text.toString()
            var phone = editTextTextPhoneNumber.text.toString()
            var email = editTextTextPersonEmail.text.toString()
            var password = editTextTextPassword.text.toString()
            var confirmPassword = editTextTextConfirmPassword.text.toString()
            var userType = 1

            if (netWorkConnected()) {
                RegistrationPresenter.registrationApiCall(firstName, lastName, phone, email, password, cntryId, stateId, cityId,
                        userType)
                Toast.makeText(this, cityId, Toast.LENGTH_SHORT).show()
            }
            else{
                showMessage("no internet connection")
            }
        }

        RegistrationPresenter.countryApi()

        toolbarid .setOnClickListener {
            var intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when(parent?.id) {
            R.id.spinnerCountry -> {
                cntryId = countryList.get(position).country_id.toInt()
                if (cntryId == 1) {
                    cntryId = 1
                    spinnerState.isEnabled = false
                    RegistrationPresenter.CountrySpinnerLoad(cntryId)
                    spinnerCountry.setSelection(0)
                }
                else if(cntryId>1) {
                    spinnerState.isEnabled = true
                    cntryId -= 1
                    RegistrationPresenter.CountrySpinnerLoad(cntryId)
                }
            }
            R.id.spinnerState -> {
                stateId = stateList.get(position).state_id.toInt()
                if (position>=1){
                    spinnerCity.isEnabled=true
                }
                else if (position<1){
                    spinnerCity.isEnabled=false
                }
                if (stateId == 1) {
                    stateId = 1
                    RegistrationPresenter.StateSpinnerLoad(stateId)
                    spinnerState.setSelection(0)
                } else if(stateId>1) {
                    stateId -= 1
                    RegistrationPresenter.StateSpinnerLoad(stateId)
                }
            }
            R.id.spinnerCity -> {
                cityId = cityList.get(position).city_id.toInt()
            }
        }
    }
    override fun onNothingSelected(parent: AdapterView<*>?) {
    }
    override fun onRegSuccess(loginBase: SignUpResponse) {
        Toast.makeText(applicationContext, loginBase.message, Toast.LENGTH_SHORT).show()
        startActivity(Intent(applicationContext, MainActivity::class.java))    }

    override fun onRegError(loginBase: com.example.mvpregistration.Models.Error) {
       // val loginBase = gson.fromJson(loginBase.errorBody()?.charStream(), Error::class.java)
        Toast.makeText(applicationContext, loginBase.message, Toast.LENGTH_SHORT).show()    }

//    override fun onRegError(loginBase: Response<JsonObject>) {
//        val loginBase = gson.fromJson(loginBase.errorBody()?.charStream(), Error::class.java)
//        Toast.makeText(applicationContext, loginBase.message, Toast.LENGTH_SHORT).show()    }

    override fun onCountrySuccess(loginBase: Response<JsonObject>) {
        val res = gson.fromJson(loginBase.body().toString(), CountryResponse::class.java)
        countryList = res.data.toTypedArray()
        var country = arrayOfNulls<String>(countryList.size)

        for (i in countryList.indices) {
            country[0] = "Select Country"
            country[i] = countryList[i].country_name
        }

        val adapter =  ArrayAdapter(this, android.R.layout.simple_spinner_item,country)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCountry.adapter = adapter
    }

    override fun onCountryError(loginBase: com.example.mvpregistration.Models.Error) {
        Toast.makeText(applicationContext, "Some error", Toast.LENGTH_SHORT).show()    }

    override fun countrySpinnerSuccess(response: Response<JsonObject>) {
        val res = gson.fromJson(response.body().toString(), StateResponse::class.java)
        stateList = res.data.toTypedArray()
        var state = arrayOfNulls<String>(stateList.size)

        for (i in stateList.indices) {
            state[0]= "Select State"

            state[i] = stateList[i].state_name
        }
        val adapter =  ArrayAdapter(this, android.R.layout.simple_spinner_item,state)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerState.adapter = adapter
    }

    override fun stateSpinnerSuccess(response: Response<JsonObject>) {
        val res = gson.fromJson(response.body().toString(), CityResponse::class.java)
    cityList = res.data.toTypedArray()
    var city = arrayOfNulls<String>(cityList.size)

        for (i in cityList.indices) {

        city[0]="Select City"
        city[i] = cityList[i].city_name

    }

    val adapter =  ArrayAdapter(this, android.R.layout.simple_spinner_item,city)
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    spinnerCity.adapter = adapter
}

//    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//        when(parent?.id) {
//            R.id.spinnerCountry -> {
//                cntryId = countryList.get(position).country_id
//                RegistrationPresenter.CountrySpinnerLoad(cntryId)
//            }
//
//            R.id.spinnerState -> {
//                stateId = stateList.get(position).state_id
//                RegistrationPresenter.StateSpinnerLoad(stateId)
//            }
//
//            R.id.spinnerCity -> {
//                cityId = cityList.get(position).city_id
//            }
//
//        }
//
//    }
//
//    override fun onNothingSelected(parent: AdapterView<*>?) {
//        TODO("Not yet implemented")
//    }


}