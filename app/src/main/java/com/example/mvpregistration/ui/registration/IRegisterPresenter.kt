package com.example.mvpregistration.ui.registration

interface IRegisterPresenter {
    fun registrationApiCall(firstName: String, lastName: String, phone: String, email:
    String, password: String, cntryId: Int, stateId: Int, cityId: Int, userType: Int)

    fun countryApi()

    fun StateSpinnerLoad(stateId: Int)

     fun CountrySpinnerLoad(cntryId: Int)
   
}