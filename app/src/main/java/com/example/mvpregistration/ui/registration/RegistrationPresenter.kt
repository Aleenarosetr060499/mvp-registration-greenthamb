package com.example.mvpregistration.ui.registration

import android.content.Context
import com.example.mvpregistration.Interface.RetrofitClient
import com.example.mvpregistration.Models.*
import com.example.mvpregistration.Models.registration.CityDetails
import com.example.mvpregistration.Models.registration.CountryDetails
import com.example.mvpregistration.Models.registration.StateDetails
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegistrationPresenter(var iRegisterView: IRegisterView, var context: Context):
    IRegisterPresenter {

    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()
    private lateinit var countryList: Array<CountryDetails>
    private lateinit var stateList: Array<StateDetails>
    private lateinit var cityList: Array<CityDetails>
    private lateinit var cntryId: String
    private lateinit var stateId: String
    private lateinit var cityId: String

    override fun registrationApiCall(
            firstName: String, lastName: String, phone: String, email:
            String, password: String, cntryId: Int, stateId: Int, cityId: Int, userType: Int
    ) {
        if (iRegisterView.netWorkConnected()) {
            RetrofitClient.instance.signUp(
                    firstName, lastName, phone, email, password, cntryId, stateId,
                    cityId, userType
            )
                    .enqueue(object : Callback<JsonObject> {
                        override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) =
                                when {
                                    response.code() == 400 -> {

                                        val loginBase = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                                        iRegisterView.onRegError(loginBase)


                                        iRegisterView.onRegError(loginBase)
                                    }
                                    response.code() == 200 -> {
                                        val loginBase = gson.fromJson(
                                                response.body().toString(),
                                                SignUpResponse::class.java
                                        )
                                        iRegisterView.onRegSuccess(loginBase)
                                    }
                                    else -> {
                                        iRegisterView.showMessage("something_went")
                                       // iRegisterView.showMessage(context.resources.getString(R.string
                                         //     .something_went))

                                    }

                                }

                        override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                        }
                    })
        } else {
            iRegisterView.showMessage("no_internet")
            //iRegisterView.showMessage(context.resources.getString(R.string.no_internet))
        }
    }

    override fun countryApi() {
        RetrofitClient.instance.countryList()
                .enqueue(object : Callback<JsonObject> {
                    override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                        when {
                            response.code() == 200 -> {
                                val res = response
                                iRegisterView.onCountrySuccess(response)
                            }
                            response.code() == 400 -> {
                                val res = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                                iRegisterView.onCountryError(res)
                            }

                        }

                    }

                    override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                        //Toast.makeText(applicationContext, "Some error", Toast.LENGTH_SHORT)
                        //.show()
                    }
                })
    }

    override fun StateSpinnerLoad(stateId: Int) {
        RetrofitClient.instance.cityList(stateId)
                .enqueue(object : Callback<JsonObject> {
                    override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                        when{

                            response.code() == 200 -> {
                                iRegisterView.stateSpinnerSuccess(response)
                            }

                            response.code() == 400 -> {
                                val res = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                            }


                        }

                    }

                    override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                    }

                })
    }

    override fun CountrySpinnerLoad(cntryId: Int) {
        RetrofitClient.instance.stateList(cntryId)
                .enqueue(object : Callback<JsonObject> {
                    override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                        when {

                            response.code() == 200 -> {
                                iRegisterView.countrySpinnerSuccess(response)
                            }

                            response.code() == 400 -> {
                                val res = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                            }
                        }
                    }

                    override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                        iRegisterView.showMessage("Some Error")
                    }

                })
    }
}


