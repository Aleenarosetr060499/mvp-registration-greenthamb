package com.example.mvpregistration.ui.homePage

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.example.mvpregistration.Models.Error
import com.example.mvpregistration.Models.homePage.productData
import com.example.mvpregistration.Models.homePage.productResponse
import com.example.mvpregistration.R
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity(),IProductView {

    private val productPresenter= productPresenter(this, this)
    private val dataList: MutableList<productData> = mutableListOf()
    private lateinit var myAdapter: ProductAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        myAdapter = ProductAdapter(dataList)

        recyclerID.layoutManager = GridLayoutManager(this,2)
        recyclerID.adapter = myAdapter

        toolbarid.title=""
        setSupportActionBar(toolbarid)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        productPresenter.getAllData()
    }

    override fun onProductSuccess(loginBase: productResponse) {
        if (loginBase.status == resources.getString(R.string.success)) {

            dataList.addAll(loginBase.data)
            myAdapter.notifyDataSetChanged()
        }    }

    override fun onProductError(error: Error) {
        Toast.makeText(applicationContext, error.message, Toast.LENGTH_LONG).show()
    }
}

