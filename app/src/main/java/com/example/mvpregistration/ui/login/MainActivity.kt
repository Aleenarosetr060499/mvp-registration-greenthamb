package com.example.mvpregistration.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.example.mvpregistration.Models.UsersList
import com.example.mvpregistration.R
import com.example.mvpregistration.ui.splashScreen.BaseActivity
import com.example.mvpregistration.ui.registration.RegistrationActivity
import com.example.mvpregistration.ui.homePage.HomeActivity
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), ILoginView {

    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()
    private val loginPresenter= LoginPresenter(this,this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        progressBarbaseactivity.visibility = View.GONE

        LoginbtnLoginbutton.setOnClickListener {
            val email = EmailLogineditText.text.toString().trim()
            val password = PasswordLogineditText.text.toString().trim()
            loginPresenter.callLoginApi(email,password,"1")
        }

        signinLogintextView.setOnClickListener {
            var intent = Intent(this, RegistrationActivity::class.java)
            startActivity(intent)
        }
    }
    override fun onBackPressed() {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)

    }

    override fun onSuccess(loginBase: UsersList) {
        showMessage(loginBase.message!!)

        if (loginBase.status=="success"){
           // SharedPreferenceMngr.getInstance(applicationContext).saveUser(loginBase.date!!)
            val intent = Intent(applicationContext, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
        else
        {
            showMessage("Something is Wrong")
        }
    }

    override fun onError(error: Error) = showMessage(error.message!!)
}
