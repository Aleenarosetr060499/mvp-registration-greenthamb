package com.example.mvpregistration.ui.splashScreen

interface MvpView {
    fun showLoading()
    fun hideLoading()
    fun netWorkConnected():Boolean
    fun showMessage(message:String)
}