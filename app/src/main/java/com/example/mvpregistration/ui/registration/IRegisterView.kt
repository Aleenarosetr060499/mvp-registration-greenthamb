package com.example.mvpregistration.ui.registration

import com.example.mvpregistration.ui.splashScreen.MvpView
import com.example.mvpregistration.Models.Error
import com.example.mvpregistration.Models.SignUpResponse
import com.google.gson.JsonObject
import retrofit2.Response

interface IRegisterView: MvpView {
    fun onRegSuccess(loginBase: SignUpResponse)
    fun onRegError(loginBase: Error)

    fun onCountrySuccess(loginBase:Response<JsonObject>)
    fun onCountryError(loginBase: com.example.mvpregistration.Models.Error)

    fun countrySpinnerSuccess(response: Response<JsonObject>)
    fun stateSpinnerSuccess(response: Response<JsonObject>)

   //fun onRegError(loginBase: Response<JsonObject>)
}