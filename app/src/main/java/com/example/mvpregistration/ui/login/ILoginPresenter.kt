package com.example.mvpregistration.ui.login

open class ILoginPresenter {
    open fun callLoginApi(emailId: String, password: String, providerType: String) {}
}
