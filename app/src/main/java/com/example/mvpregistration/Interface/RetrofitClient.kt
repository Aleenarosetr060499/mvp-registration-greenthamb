package com.example.mvpregistration.Interface

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {
    private const val BASE_URL = "https://www.mylittlegreenthumb.in/apiv1/"
    private val okHttpClient: OkHttpClient = OkHttpClient.Builder().addInterceptor { chain ->
        val original = chain.request()

        val requesBuilder = original.newBuilder().addHeader(
            "Authorization",
            "ZDJWaUlIUmxjM1F0WWpKU2JHTnBRblpqYVVKcldsZE9kbHBIVm5sSlIyeDE"
        ).method(
            original
                .method(), original.body()
        )

        val request = requesBuilder.build()
        chain.proceed(request)
    }.build()

    val instance: Api by lazy {
        val retrofit =
            Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create())
                .client(
                    okHttpClient
                ).build()
        retrofit.create(Api::class.java)
    }
}