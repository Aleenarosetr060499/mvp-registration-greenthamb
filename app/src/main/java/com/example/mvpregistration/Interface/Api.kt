package com.example.mvpregistration.Interface

import com.example.mvpregistration.Models.homePage.productData
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface Api {
    //login
    @FormUrlEncoded
    @POST("login")
    fun login(@Field("email_id") email_id: String, @Field("password") password: String,
              @Field("user_type") user_type: Int): Call<JsonObject>

    //registration

    @POST("country")
    fun countryList():Call<JsonObject>

    @FormUrlEncoded
    @POST("state")
    fun stateList(
        @Field("country_id") countryIDAPI:Int
    ) : Call<JsonObject>

    @FormUrlEncoded
    @POST("city")
    fun cityList(
        @Field("state_id") stateIDAPI:Int
    ) : Call<JsonObject>


    @FormUrlEncoded
    @POST("registration")
    fun signUp(
            @Field("first_name") first_name:String,
            @Field("last_name") last_name:String,
            @Field("user_phone1") user_phone1:String,
            @Field("email_id") email_id:String,
            @Field("password") password:String,
            @Field("country") country: Int,
            @Field("state") state: Int,
            @Field("city") city: Int,
            @Field("user_type") user_type: Int
    ) : Call<JsonObject>

    //homepage

   //@FormUrlEncoded
    @GET("productlist ")
    fun productList(): Call<List<JsonObject>>
}