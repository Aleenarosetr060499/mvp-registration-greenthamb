package com.example.mvpregistration.Models

import com.example.mvpregistration.Models.login.Users

data class UsersList (val status: String? = null,
                      val message: String? = null,
                      val date: Users? = null)

data class  Error(val status: String?,val message: String?) {
   
}
