package com.example.mvpregistration.Models.registration


data class CountryDetails(
    val country_id: String,
    val country_name: String
)
