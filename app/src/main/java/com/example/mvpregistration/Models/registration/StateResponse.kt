package com.example.mvpregistration.Models.registration

import com.example.mvpregistration.Models.registration.StateDetails

data class StateResponse (
    val data: List<StateDetails>,
    val message: String,
    val status: String
)