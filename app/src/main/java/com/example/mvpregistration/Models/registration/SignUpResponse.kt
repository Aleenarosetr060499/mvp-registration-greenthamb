package com.example.mvpregistration.Models

import com.example.mvpregistration.Models.registration.SignUpDetails

data class SignUpResponse(
    val data: SignUpDetails,
    val message: String,
    val status: String
)
data class  RegError(val status: String?,val message: String?)
