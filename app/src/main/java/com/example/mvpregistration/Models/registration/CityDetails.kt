package com.example.mvpregistration.Models.registration

data class CityDetails(
    val city_id: String,
    val city_name: String
)