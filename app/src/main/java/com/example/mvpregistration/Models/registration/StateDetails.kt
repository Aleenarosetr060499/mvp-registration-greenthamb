package com.example.mvpregistration.Models.registration
data class StateDetails (
    val state_id: String,
    val state_name: String
)