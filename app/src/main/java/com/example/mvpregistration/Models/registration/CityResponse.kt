package com.example.mvpregistration.Models.registration

import com.example.mvpregistration.Models.registration.CityDetails

data class CityResponse(
    val data: List<CityDetails>,
    val message: String,
    val status: String
)