package com.example.mvpregistration.Models.registration

import com.example.mvpregistration.Models.registration.CountryDetails

data class CountryResponse(
    val data: List<CountryDetails>,
    val message: String,
    val status: String
)



//data class Error(val status: String?,val message: String?)